/*package ar.edu.unq.cpi.libromatriz.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class InscripcionAFinalTest {
	
	private Materia lecturaYEscrituraAcademica;
	private Materia matematica;
	private Materia elementosDeProgramacionYLogica;
	private Materia introduccionALaProgramacion;
	private Materia organizacionDeComputadoras;
	private Materia matematica1;
	private Materia programacionConObjetos1;
	private Materia baseDeDatos;
	private Materia estructuraDeDatos;
	private Materia programacionConObjetos2;
	private Materia redesDeComputadoras;
	private Materia sistemasOperativos;
	private Materia programacionConcurrente;
	private Materia matematica2;
	private Materia elementosDeIngenieriaDeSoftware;
	private Materia construccionDeInterfacesDeUsuario;
	private Materia estrategiasDePersistencia;
	private Materia programacionFuncional;
	private Materia desarrolloDeAplicaciones;
	private Materia laboratorioDeSistemasOperativosYRedes;
	private Cursada lecturaYEscrituraAcademicaMario;
	private Cursada matematicaMario;
	private Cursada elementosDeProgramacionYLogicaMario;
	private Cursada introduccionALaProgramacionMario;
	private Alumno mario;
	private Cursada organizacionDeComputadorasMario;
	private Cursada matematica1Mario;
	private Cursada programacionConObjetos1Mario;


	@Before
	public void setUp() throws Exception{
		
		// Creo una carrera
		Carrera tecnicaturaEnProgramacionInformatica = new Carrera("Tecnicatura en Programación Informática", "2015", 4.5);
		
		lecturaYEscrituraAcademica = new Materia(tecnicaturaEnProgramacionInformatica, "Lectura Y Escritura Académica", true);
		matematica = new Materia(tecnicaturaEnProgramacionInformatica, "Matemáticas", true);
		elementosDeProgramacionYLogica = new Materia(tecnicaturaEnProgramacionInformatica, "Elementos de Programación y Lógica", true);
		introduccionALaProgramacion = new Materia(tecnicaturaEnProgramacionInformatica, "Introducción a la Programación", false);
		organizacionDeComputadoras = new Materia(tecnicaturaEnProgramacionInformatica, "Organización de Computadoras", false);
		matematica1 = new Materia(tecnicaturaEnProgramacionInformatica, "Matemática I", false);
		programacionConObjetos1 = new Materia(tecnicaturaEnProgramacionInformatica, "Programación con Objetos I", false);
		baseDeDatos = new Materia(tecnicaturaEnProgramacionInformatica, "Base de Datos", false);
		estructuraDeDatos = new Materia(tecnicaturaEnProgramacionInformatica, "Estructura de Datos", false);
		programacionConObjetos2 = new Materia(tecnicaturaEnProgramacionInformatica, "Programación con Objetos II", false);
		redesDeComputadoras = new Materia(tecnicaturaEnProgramacionInformatica, "Redes de Computadoras", false);
		sistemasOperativos = new Materia(tecnicaturaEnProgramacionInformatica, "Sistemas Operativos", false);
		programacionConcurrente = new Materia(tecnicaturaEnProgramacionInformatica, "Programación Concurrente", false);
		matematica2 = new Materia(tecnicaturaEnProgramacionInformatica, "Matemática II", false);
		elementosDeIngenieriaDeSoftware = new Materia(tecnicaturaEnProgramacionInformatica, "Elementos de Ingeniería de Software", false);
		construccionDeInterfacesDeUsuario = new Materia(tecnicaturaEnProgramacionInformatica, "Construcción de Interfaces de Usuario", false);
		estrategiasDePersistencia = new Materia(tecnicaturaEnProgramacionInformatica, "Estrategias de Persistencia", false);
		programacionFuncional = new Materia(tecnicaturaEnProgramacionInformatica, "Programación Funcional", false);
		desarrolloDeAplicaciones = new Materia(tecnicaturaEnProgramacionInformatica, "Desarrollo de Aplicaciones", false);
		laboratorioDeSistemasOperativosYRedes = new Materia(tecnicaturaEnProgramacionInformatica, "Laboratorio de Sistemas Operativos y Redes", false);
		
		// Agrego materias correlativas para una materia
		introduccionALaProgramacion.agregarCorrelativa(lecturaYEscrituraAcademica);
		introduccionALaProgramacion.agregarCorrelativa(matematica);
		introduccionALaProgramacion.agregarCorrelativa(elementosDeProgramacionYLogica);
		
		programacionConObjetos1.agregarCorrelativa(introduccionALaProgramacion);
		
		// Creo alumnos
		Alumno nahuel = new Alumno("Nahuel", "Martínez", "39449222");
		Alumno juan = new Alumno("Juan Cruz", "Santamaría", "38951524");
		Alumno fede = new Alumno("Federico", "Ferreyra Lombardi", "39146980");
		Alumno natalia = new Alumno("Natalia", "Marzec", "29026816");
		mario = new Alumno("Mario", "Gudiño","30773213");
		
		// Agrego a los alumnos a la carrera
		tecnicaturaEnProgramacionInformatica.agregarAlumno(nahuel);
		tecnicaturaEnProgramacionInformatica.agregarAlumno(juan);
		tecnicaturaEnProgramacionInformatica.agregarAlumno(fede);
		tecnicaturaEnProgramacionInformatica.agregarAlumno(natalia);
		tecnicaturaEnProgramacionInformatica.agregarAlumno(mario);
		
		// Creo las cursadas
		lecturaYEscrituraAcademicaMario = new Cursada(lecturaYEscrituraAcademica, EstadoCursada.APROBADA_PROMOCION);
		mario.agregarCursada(lecturaYEscrituraAcademicaMario);
		
		matematicaMario = new Cursada(matematica, EstadoCursada.APROBADA_PROMOCION);
		mario.agregarCursada(matematicaMario);
		
		elementosDeProgramacionYLogicaMario = new Cursada(elementosDeProgramacionYLogica, EstadoCursada.APROBADA_PROMOCION);
		mario.agregarCursada(elementosDeProgramacionYLogicaMario);
		
		introduccionALaProgramacionMario = new Cursada(introduccionALaProgramacion, EstadoCursada.CURSANDO);
		mario.agregarCursada(introduccionALaProgramacionMario);
		
		organizacionDeComputadorasMario = new Cursada(organizacionDeComputadoras, EstadoCursada.REGULARIZADA);
		mario.agregarCursada(organizacionDeComputadorasMario);
		
		matematica1Mario = new Cursada(matematica1, EstadoCursada.APROBADA_FINAL);
		mario.agregarCursada(matematicaMario);
		
		programacionConObjetos1Mario = new Cursada(programacionConObjetos1, EstadoCursada.DESAPROBADA);
		mario.agregarCursada(programacionConObjetos1Mario);
	}
	
	@Test
	public void laCursadaEstaAprobada() {
		assertTrue(lecturaYEscrituraAcademicaMario.estaAprobada()); // Aprobada por promoción
		assertTrue(matematica1Mario.estaAprobada()); // Aprobada por final
	}
	
	@Test
	public void laCursadaNoEstaAprobada() {
		assertFalse(introduccionALaProgramacionMario.estaAprobada()); // Está cursando
		assertFalse(organizacionDeComputadorasMario.estaAprobada()); // Está regularizada
	}
	
	@Test
	public void lasCursadasEstanEnCursoORegularizadas() {
		assertEquals(Arrays.asList(introduccionALaProgramacionMario, organizacionDeComputadorasMario), mario.getCursadasEnCursoORegularizadasPor("30773213"));
	}
	
	@Test
	public void lasCursadasNoEstanEnCursoORegularizadas() {
		assertNotEquals(Arrays.asList(lecturaYEscrituraAcademicaMario, matematicaMario, elementosDeProgramacionYLogicaMario, matematica1Mario, programacionConObjetos1Mario), mario.getCursadasEnCursoORegularizadasPor("30773213"));
	}
	
	@Test
	public void puedeInscribirseAFinalDeMateriasCuyasCorrelativasEstanAprobadas() {
		assertTrue(mario.puedeInscribirseAFinalDe(introduccionALaProgramacion));
	}
	
	@Test
	public void noPuedeInscribirseAFinalDeMateriasCuyasCorrelativasNoEstanAprobadas() {
		assertFalse(mario.puedeInscribirseAFinalDe(programacionConObjetos1));
	}
	
}
*/