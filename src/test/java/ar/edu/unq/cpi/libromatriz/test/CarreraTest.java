/*package ar.edu.unq.cpi.libromatriz.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;

public class CarreraTest {

	private Carrera informatica;
	private Carrera contador;
	private Carrera diseno;

	public CarreraTest() {

	}

	@Before
	public void setUp() {
		informatica = new Carrera("tecnicatura en informatica", "1270/8", 5);
		contador = new Carrera("Contador", "12580/9", 6);
		diseno = new Carrera("Diseño grafico", "1350/17", 5);

		OfertaAcademica.instituto().addCarreraAlListado(informatica);
		OfertaAcademica.instituto().addCarreraAlListado(contador);
		OfertaAcademica.instituto().addCarreraAlListado(diseno);
	}

	@Test
	public void trasArchivarCarreraNoApareceEnElListado() {
		OfertaAcademica.instituto().archivarCarrera(diseno);

		assertEquals(Arrays.asList(informatica, contador), OfertaAcademica.instituto().getCarreras());
		assertEquals(Arrays.asList(diseno), OfertaAcademica.instituto().getCarrerasArchivadas());
	}

	@Test
	public void ArchivandoLaCarreraApareceEnElListadoDeCarrerasArchivadas() {
		OfertaAcademica.instituto().archivarCarrera(diseno);

		assertEquals(Arrays.asList(diseno), OfertaAcademica.instituto().getCarrerasArchivadas());

	}

	@Test
	public void eliminandoUnaCarreraArchivadaDejaDeAparecenEnElListadoDeCarrerasArchivadas() {
		OfertaAcademica.instituto().archivarCarrera(informatica);
		OfertaAcademica.instituto().archivarCarrera(contador);
		OfertaAcademica.instituto().borrarCarrera(informatica);

		assertEquals(Arrays.asList(contador), OfertaAcademica.instituto().getCarrerasArchivadas());
	}
	
	@Test
	public void AlDesarchivarUnaCarreraVuelveAlListadoDeCarrerasActivas(){
		OfertaAcademica.instituto().archivarCarrera(informatica);
		OfertaAcademica.instituto().archivarCarrera(contador);
		OfertaAcademica.instituto().activarCarreraArchivada(contador);
		
		assertEquals(Arrays.asList(diseno,contador), OfertaAcademica.instituto().getCarreras());
	}
}
*/