package ar.edu.unq.cpi.libromatriz.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class MatriculacionTest {
	
	private Alumno alumno;
	private Carrera tecnicoProgramador;
	private Materia intro;
	private Materia objetos;
	private Materia objetos2;
	private Materia objetos3;

	@Before
	public void setUp() throws Exception{
		alumno = new Alumno("Juan","Adamkzick","21888888");
		tecnicoProgramador = new Carrera("Técnico programador", "123332", 4);
		intro = new Materia("Intro", true);
		objetos = new Materia("Objetos", true);
		objetos2 = new Materia("Objetos 2", true);
		objetos3 = new Materia("Objetos 3", true);
		tecnicoProgramador.agregarMateria(intro);
		tecnicoProgramador.agregarMateria(objetos);
		alumno.addCarreras(tecnicoProgramador);
		objetos2.agregarCorrelativa(objetos);
		objetos3.agregarCorrelativa(objetos2);
		
	}
	
	@Test
	public void materiasNoCursadasParaUnAlumnoYUnaCarrera(){
		
		Cursada cursada1 = new Cursada(EstadoCursada.CURSANDO);
		cursada1.setMateria(intro);
		
		alumno.agregarCursada(cursada1);
		Set<Materia> materias = new HashSet<>();
		materias.add(objetos);
		
		assertEquals(materias, alumno.getMateriasPorCursarEnCarrera(tecnicoProgramador));
	}

	@Test
	public void NoPuedeMatricularseAObjetos2(){
		alumno.addCarreras(tecnicoProgramador);
		
		assertFalse(alumno.puedeMatricularseA(objetos2));
	}
	
	@Test
	public void puedeMatricularseAObjetos2PorqueObjetosEstaRegularizada(){
		Cursada cursadaObjetos1 = new Cursada(EstadoCursada.REGULARIZADA);
		cursadaObjetos1.setMateria(objetos);
		alumno.agregarCursada(cursadaObjetos1);
		
		assertTrue(alumno.puedeMatricularseA(objetos2));
	}
	
	@Test
	public void noPuedeMatricularseAObjetos2PorqueEstaCursandoObjetos1(){
		Cursada cursadaObjeto1 = new Cursada(EstadoCursada.CURSANDO);
		cursadaObjeto1.setMateria(objetos);
		alumno.agregarCursada(cursadaObjeto1);
		
		assertFalse(alumno.puedeMatricularseA(objetos2));
	}
	
	@Test
	public void noPuedeMatricularseAObjetos2PorqueNoCursoObjetos2(){
		Cursada cursadaObjeto1 = new Cursada(EstadoCursada.REGULARIZADA);
		cursadaObjeto1.setMateria(objetos);
		alumno.agregarCursada(cursadaObjeto1);
		
		assertFalse(alumno.puedeMatricularseA(objetos3));
	}
	
	@Test
	public void puedeMatricularseAObjetos3PorqueRegularizoObjetos2Y1(){
		Cursada cursadaObjeto1 = new Cursada(EstadoCursada.REGULARIZADA);
		cursadaObjeto1.setMateria(objetos);
		Cursada cursadaObjetos2 = new Cursada(EstadoCursada.REGULARIZADA);
		cursadaObjetos2.setMateria(objetos2);
		alumno.agregarCursada(cursadaObjeto1);
		alumno.agregarCursada(cursadaObjetos2);
		
		assertTrue(alumno.puedeMatricularseA(objetos3));
	}
	
	
}
