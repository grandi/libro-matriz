package ar.edu.unq.cpi.libromatriz.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;


public class CorrelatividadTest {

	@Test
	public void agregarCorrelativaandaTest(){
		Carrera prog = new Carrera();
		
		Materia mate = new Materia("matematica 1", false);
		Materia intro = new Materia ("introduccion a programacion", false);
		Materia obj = new Materia ("Objetos 1", true);
	
		prog.agregarMateria(mate);
		prog.agregarMateria(obj);
		prog.agregarMateria(intro);
		
		obj.agregarCorrelativa(intro);
		obj.agregarCorrelativa(mate);
		
		assertEquals(new HashSet<>(Arrays.asList(intro, mate)), obj.getCorrelativas());
		}	
}
