package ar.edu.unq.cpi.libromatriz.epers;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Direccion;
import ar.edu.unq.cpi.libromatriz.modelo.Docente;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class GenerateData {
	public static void main(String[] args) {

		SessionFactoryContainer.buildSessionFactory(true);
		Session s = SessionFactoryContainer.getSessionFactory().getCurrentSession();

		Transaction transaction = s.beginTransaction();

		try {
			Docente doc1 = new Docente("leo", "gassman", "0112155");
			Docente doc2 = new Docente("pablo", "niellod", "01154554");

			Direccion dirUno = new Direccion("palacios", "1080", "capitan sarmiento");
			Direccion dirDos = new Direccion("salta", "160", "capitan sarmiento");
			doc1.setDireccion(dirUno);
			doc2.setDireccion(dirDos);

			Carrera programacion = new Carrera("tecnico en programacion", "1205/18", 5);
			Carrera matematicas = new Carrera("profesorado matematica", "158/16", 6);

			Materia mate = new Materia("matematica", false);
			Materia intro = new Materia("Introducción a la programación", true);
			Materia estructura = new Materia("Estructuras de datos", true);
			Materia objetos = new Materia("Programación con objetos I", false);
			
			mate.agregarCorrelativa(estructura);
			mate.agregarCorrelativa(objetos);
			
			objetos.agregarCorrelativa(intro);

			Alumno alu1 = new Alumno("valeria", "grandi", "27826286", "2478-444436", dirUno, "vaf@hotmail.com");
			Alumno alu2 = new Alumno("natalia", "marzec", "27545426", "2478-464776", dirDos, "naty@hotmail.com");

			programacion.agregarMateria(mate);
			programacion.agregarMateria(estructura);
			programacion.agregarMateria(objetos);
			matematicas.agregarMateria(intro);
			matematicas.agregarMateria(mate);
			alu1.setCarrera(matematicas);
			alu2.setCarrera(programacion);
			alu2.setCarrera(matematicas);

			 matematicas.agregarAlumno(alu2);
			 matematicas.agregarAlumno(alu1);

			 programacion.agregarAlumno(alu1);
			// matematicas.agregarAlumno(alu2);
			// matematicas.agregarAlumno(alu1);
			// DocenteHome.getInstance().insert(doc1);
			// DocenteHome.getInstance().insert(doc2);
			// DireccionHome.getInstance().insert(dirUno);
			// DireccionHome.getInstance().insert(dirDos);
			AlumnoHome.getInstance().insert(alu1);
			AlumnoHome.getInstance().insert(alu2);
			MateriaHome.getInstance().insert(mate);
			MateriaHome.getInstance().insert(objetos);
			MateriaHome.getInstance().insert(estructura);
			MateriaHome.getInstance().insert(intro);
			// DireccionHome.getInstance().insert(dirDos);

			CarreraHome.getInstance().insert(programacion);
			CarreraHome.getInstance().insert(matematicas);

			AlumnoHome.getInstance().insert(alu1);
			transaction.commit();
		} catch (RuntimeException e) {
			transaction.rollback();
			throw e;
		} finally {
			s.close();
			SessionFactoryContainer.getSessionFactory().close();
		}

	}

	protected static void setUp() throws Exception {
		// A SessionFactory is set up once for an application!
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();

		try {
			SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception e) {
			// The registry would be destroyed by the SessionFactory, but we had
			// trouble
			// building the SessionFactory
			// so destroy it manually.
			StandardServiceRegistryBuilder.destroy(registry);
			throw e;
		}
	}

	@Override
	public String toString() {
		return "GenerateData [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
