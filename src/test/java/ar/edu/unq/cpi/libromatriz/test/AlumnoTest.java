package ar.edu.unq.cpi.libromatriz.test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.Direccion;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.wicket.alumno.AlumnoStore;

public class AlumnoTest {
	
	
AlumnoStore store;
Alumno delfi;Alumno leo;Alumno david;Alumno juan;Alumno anto;Alumno pato;Alumno adri;

Cursada cursadaUno;

Direccion direccion1;Direccion direccion2;




	@Before
	public void setUp() throws Exception{
		
		delfi=new Alumno("delfi", "baldaccino","25999000");
		leo=new Alumno("leo", "marzec", "30888777");
		david=new Alumno("david", "escudero", "32000444");
		juan= new Alumno("Juan","Adamkzick","21888888");
		anto= new Alumno("Anto","Baldaccino","32000444");
		
		direccion1=new Direccion("BlascoIbañez", "839","Adrogué");
		pato=new Alumno("pato", "marzec", "25000999","15222999",direccion1,"pato99@gmail.com");
		pato.setDni("25000999");
		pato.getDni();
		
		direccion2=new Direccion("PiedraBuena","1590","José Marmol");
		adri= new Alumno("adri", "lopez", "27111222","15777888",direccion2,"adrilopez@gmail.com");
		adri.setDni("27111222");
		adri.getDni();
		
		cursadaUno=new Cursada();
		
		AlumnoStore.unico().agregarAlumno(adri);
		AlumnoStore.unico().agregarAlumno(pato);
		
		
					
	}

	//Test para Matriculación y Inscripción a Final
	@Test
	public void obtenerAlumnoAgregadoSegunDniMario(){
		Alumno mario = new Alumno("Mario", "Gudiño", "30773213");
		AlumnoStore.unico().agregarAlumno(mario);
		assertEquals(mario, AlumnoStore.unico().getAlumnoConDni("30773213"));
	}
	
	@Test
	public void pruebaAlumno(){
		
		cursadaUno.setEstado(EstadoCursada.CURSANDO);
		cursadaUno.getEstado();
		assertTrue(cursadaUno.estadoCursando());
		david.setDni("32000444");
		cursadaUno.cursaAlumnoConDni("32000444");
	
	
		david.getMateriasCursadas();
		david.getMateriasCursadasPor("32000444");
		AlumnoStore.unico().agregarAlumno(david);
		AlumnoStore.unico().getAlumnos();
		AlumnoStore.unico().getAlumnoConDni("32000444");
		
	   Carrera programacion= new Carrera("Programcion", "300", 4);
	   AlumnoStore.unico().agregarCarrera(programacion);
	   AlumnoStore.unico().getCarrerasDe(david);
		
	}
	
	
}











