package ar.edu.unq.cpi.libromatriz.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Test;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

import ar.edu.unq.cpi.libromatriz.modelo.Materia;


public class MateriaTest {

	@Test
	public void puedeTenerUnNombre() {
		Carrera tip=new Carrera();
		Materia mate = new Materia( "mate", false);
		Materia didactica = new Materia( "Didáctica I", false);
		tip.agregarMateria(didactica);
		
		assertEquals(Arrays.asList(didactica), tip.getMaterias());
		assertTrue(tip.puedoAgregarMateria(mate));
	}

	
}

