package ar.edu.unq.cpi.libromatriz.wicket;

import org.apache.wicket.Page;
import org.apache.wicket.protocol.http.WebApplication;

import ar.edu.unq.cpi.libromatriz.materia.MateriasStore;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;
import ar.edu.unq.cpi.libromatriz.wicket.alumno.AlumnoStore;

public class LibroMatrizApplication extends WebApplication {

	@Override
	public Class<? extends Page> getHomePage() {
		return HomePage.class;
	}

	@Override
	protected void init() {
		super.init();
		this.getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
		this.getRequestCycleSettings().setResponseRequestEncoding("UTF-8"); 
		
		OfertaAcademica.instituto().carreraInicial();
		MateriasStore.unico().cargarDatosIniciales();
		AlumnoStore.unico().cargarDatosIniciales();
	}
}
