package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;


public class AgregarAlumnoPage extends WebPage{
	
	private static final long serialVersionUID = 8087064313539032039L;
    private AgregarAlumnoController controller = new AgregarAlumnoController();

	public AgregarAlumnoPage(){
		super();
		this.agregarForm();
		this.agregarBotones();
	}

	private void agregarForm() {
		Form<AgregarAlumnoController> crearAlumnoForm = new Form<AgregarAlumnoController>("crearAlumnoForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				AgregarAlumnoPage.this.controller.agregarAlumno();
				this.setResponsePage(new ListadoAlumnoPage());
			
			}	
			
		};
					
		crearAlumnoForm.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		crearAlumnoForm.add(new TextField<>("apellido", new PropertyModel<>(this.controller, "apellido")));
		crearAlumnoForm.add(new TextField<>("dni", new PropertyModel<>(this.controller,"dni")));
		crearAlumnoForm.add(new TextField<>("telefono",new PropertyModel<>(this.controller,"telefono")));
		crearAlumnoForm.add(new TextField<>("calle",new PropertyModel<>(this.controller,"calle")));
		crearAlumnoForm.add(new TextField<>("numero",new PropertyModel<>(this.controller, "numero")));
		crearAlumnoForm.add(new TextField<>("localidad",new PropertyModel<>(this.controller, "localidad")));
		crearAlumnoForm.add(new TextField<>("email",new PropertyModel<>(this.controller, "email")));
		
		
		this.add(crearAlumnoForm);
	}

	private void agregarBotones() {
		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = -5103954084634722612L;

			@Override
			public void onClick() { this.setResponsePage(new ListadoAlumnoPage()); }
		});
	}
}