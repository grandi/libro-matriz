package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

public class ListadoAlumnoController implements Serializable {
	private static final long serialVersionUID = 6824558483893635503L;
	private List<Alumno> alumnos;
	private Alumno alumno;

	public ListadoAlumnoController() {
		this.alumnos = AlumnoStore.unico().getAlumnos();
	}

	public ListadoAlumnoController(Carrera carrera1) {
		this.alumnos = carrera1.getAlumnosInscriptos();
	}

	public List<Alumno> getAlumnos() {
		return this.alumnos.stream().sorted(Comparator.comparing(Alumno::getNombreCompleto))
				.collect(Collectors.toList());
	}

	public void setAlumno(Alumno _alumno){
		this.alumno=_alumno;
	}
	public Alumno getAlumno() {
		return alumno;
	}
	public List<Alumno> getDatosDe(String dni) {//agregado 6 de mayo
		return this.getAlumnos().stream()
				.filter(a -> a.getDni().equals(dni))
				.collect(Collectors.toList());
	}
}
