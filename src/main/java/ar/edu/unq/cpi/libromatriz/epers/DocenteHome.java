package ar.edu.unq.cpi.libromatriz.epers;

import ar.edu.unq.cpi.libromatriz.modelo.Docente;

public class DocenteHome extends HomeGeneralSession<Object> {

	private static final long serialVersionUID = 4775910097257163038L;

	static private DocenteHome instance = new DocenteHome();

	public static DocenteHome getInstance() {
		return instance;
	}

	@Override
	public Docente findByName(String name) {
		return this.getSession().createQuery("FROM Docente WHERE nombre = :name", Docente.class)
				.setParameter("name", name).getSingleResult();

	}

}
