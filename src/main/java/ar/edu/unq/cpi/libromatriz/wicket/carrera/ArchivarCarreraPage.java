package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

public class ArchivarCarreraPage extends WebPage {
	private static final long serialVersionUID = 1L;

	private ArchivarCarreraController controller;

	public ArchivarCarreraPage() {

	}

	public ArchivarCarreraPage(Carrera carrera1) {

		this.controller = new ArchivarCarreraController(carrera1);
		this.archivarCarrera();

	}

	private void archivarCarrera() {
		Form<CarreraController> carreraArchivada = new Form<CarreraController>("archivarCarrera") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {

				ArchivarCarreraPage.this.controller.archivarCarrera();
				this.setResponsePage(new ListadoDeCarreras());
			}
		};
		carreraArchivada.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		carreraArchivada.add(new TextField<>("resolucion", new PropertyModel<>(this.controller, "resolucion")));
		carreraArchivada.add(new TextField<>("duracion", new PropertyModel<>(this.controller, "duracion")));

		this.add(carreraArchivada);

		carreraArchivada.add(new Link<String>("cancelar") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());
			}

		});
	}
}