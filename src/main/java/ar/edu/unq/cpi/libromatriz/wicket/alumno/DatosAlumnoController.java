package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.Direccion;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;



public class DatosAlumnoController implements Serializable {

	private static final long serialVersionUID = 6785848824087742754L;

	private Alumno alumno;
	private String nombre;
	private String apellido;
	private String dni;
	private String telefono;
	private String calle;
	private String numero;
	private String localidad;
	private String email;
	private Direccion direccion;
	private List<Cursada> cursadas= new ArrayList<>();
	private EstadoCursada estado;
	private List<Carrera> carreras= new ArrayList<>();
	private Carrera carrera;
	
	
	public DatosAlumnoController(){
		super();
	}
	public DatosAlumnoController(Alumno _alumno){
		this.alumno=_alumno;
		this.nombre=_alumno.getNombre();
		this.apellido=_alumno.getApellido();
		this.dni=_alumno.getDni();
		this.telefono=_alumno.getTelefono();
		this.calle=_alumno.getDireccion().getCalle();
		this.numero=_alumno.getDireccion().getNumero();
		this.localidad=_alumno.getDireccion().getLocalidad();
		this.email=_alumno.getEmail();
		
		
	}
	public void setDatos(){
		alumno.setApellido(this.getApellido());
		alumno.setNombre(this.getNombre());
		alumno.setDni(this.getDni());
		alumno.setTelefono(this.getTelefono());
		alumno.setDireccion(new Direccion(this.getCalle(),this.getNumero(),this.getLocalidad()));
		alumno.setEmail(this.getEmail());
	}
	
	
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}
	public void setAlumno(Alumno _alumno){
		this.alumno=_alumno;
	}

	public String getNombre() {
		return this.nombre;
	}

	public String getApellido() {
		return this.apellido;
	}

	public String getDni() {
		return this.dni;
	}


	public void setNombre(String _nombre) {
		this.nombre = _nombre;
	}

	public void setApellido(String _apellido) {
		this.apellido = _apellido;
	}

	public void setDni(String _dni) {
		this.dni = _dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<Cursada> getCursadas() {
		return cursadas;
	}

	public void setCursadas(List<Cursada> cursadas) {
		this.cursadas = cursadas;
	}

	public List<Carrera> getCarreras() {
		return this.getAlumno().getCarreras();
	}

	public void setCarreras(List<Carrera> carreras) {
		this.carreras = carreras;
	}

	public EstadoCursada getEstado() {
		return estado;
	}

	public void setEstado(EstadoCursada estado) {
		this.estado = estado;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	
	public void modificarAlumno(){
		if(this.datosAlumnoBienCargados()){
			this.setDatos();
		}
	}
	
	public void removerAlumno() {
		if (this.datosAlumnoBienCargados()) {
			Direccion direccion=new Direccion(this.getCalle(), this.getNumero(),this.getLocalidad());
			Alumno alumno = new Alumno(this.getNombre(), this.getApellido(), this.getDni(),
					        this.getTelefono(),direccion,this.getEmail());
			
			AlumnoStore.unico().setBajaAlumno(alumno);

		}
	}

	private boolean datosAlumnoBienCargados() {
		return this.getNombre() != null && this.getApellido() != null 
				&& this.getDni() != null &&this.getTelefono()!=null &&
				this.getCalle()!= null && this.getNumero()!=null &&this.getLocalidad()!=null
				&& this.getEmail()!=null;
	}
	public Carrera getCarrera() {
		return carrera;
	}
	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
	
	

}
