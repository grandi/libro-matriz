package ar.edu.unq.cpi.libromatriz.materia;

import java.util.ArrayList;

import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;

public class MateriasStore {
	private static MateriasStore elUnico = new MateriasStore();

	public static MateriasStore unico() {
		if (elUnico == null) {
			elUnico = new MateriasStore();
		}
		return elUnico;
	}

	private List<Materia> materias = new ArrayList<>();
		
	public MateriasStore(){
		super();
	}

	public void agregarMateria(Materia carreraN) {
		materias.add(carreraN);
	}
	public List<Materia> getMaterias() {
		return materias;
	}
	public void setMaterias(List<Materia> mate) {
		this.materias = mate;
	}

	public void cargarDatosIniciales() {
		Carrera tpi = OfertaAcademica.instituto().getCarreraConNombre("tecnicatura en informatica");
		Materia intro = new Materia("Introducción a la programación", true);
		Materia mate = new Materia("Matematica 1", true);
		Materia estructura = new Materia("Estructuras de datos", true);
		Materia objetos = new Materia("Programación con objetos I", false);
		
		tpi.agregarMateria(intro);
		tpi.agregarMateria(mate);
		tpi.agregarMateria(estructura);
		tpi.agregarMateria(objetos);
		
		objetos.agregarCorrelativa(intro);
		objetos.agregarCorrelativa(mate);
		estructura.agregarCorrelativa(objetos);
	}
	
}
