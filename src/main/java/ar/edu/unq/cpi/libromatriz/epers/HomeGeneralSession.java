package ar.edu.unq.cpi.libromatriz.epers;

import org.hibernate.Session;

public abstract class HomeGeneralSession<T> implements Home<T>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Session getSession() {
		return SessionFactoryContainer.getSessionFactory().getCurrentSession();
	}
	
	@Override
	public void insert(Object object) {
this.getSession().save(object);		
	}

	@Override
	public void update(Object object) {
		this.getSession().update(object);
	}

	@Override
	public void delete(Object object) {
		this.getSession().delete(object);
	}

}
