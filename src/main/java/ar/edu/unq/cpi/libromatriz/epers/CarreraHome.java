package ar.edu.unq.cpi.libromatriz.epers;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

public class CarreraHome extends HomeGeneralSession<Object> {

	private static final long serialVersionUID = 1L;
	static private CarreraHome instance = new CarreraHome();
	
	public static CarreraHome getInstance(){
		return instance;
	}
	
	@Override
	public Carrera findByName(String name) {
		return this.getSession().createQuery("FROM Carrera WHERE nombre = :name", Carrera.class)
				.setParameter("name", name).getSingleResult();
	}

}
