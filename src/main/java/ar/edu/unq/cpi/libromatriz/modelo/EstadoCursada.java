package ar.edu.unq.cpi.libromatriz.modelo;

public enum EstadoCursada {
	CURSANDO, REGULARIZADA, APROBADA_FINAL, APROBADA_PROMOCION, DESAPROBADA
}
