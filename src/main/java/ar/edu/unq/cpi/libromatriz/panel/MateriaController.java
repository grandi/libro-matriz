package ar.edu.unq.cpi.libromatriz.panel;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.wicket.HomePage;
import ar.edu.unq.cpi.libromatriz.wicket.matriculacion.NoPudoMatricularsePage;
import ar.edu.unq.cpi.libromatriz.wicket.matriculacion.PudoMatricularsePage;

public class MateriaController {
	private Carrera carrera;
	private Materia materia;
	private Alumno alumno;
	
	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	
	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	public List<Materia> getMateriasNoCursadas(){
		List<Materia> materiasList = new ArrayList<>(this.getAlumno().getMateriasPorCursarEnCarrera(this.getCarrera()));
		return materiasList;
	}
	
	public void matricular(HomePage page){
		if (getAlumno()
				.puedeMatricularseA(getMateria()))
		{
			Cursada cursada = new Cursada(EstadoCursada.CURSANDO);
			cursada.setMateria(getMateria());
			
			getAlumno()
			.agregarCursada(cursada);
			page.setResponsePage(new PudoMatricularsePage(getAlumno(),getMateria()));
		}else{
			page.setResponsePage(new NoPudoMatricularsePage(getAlumno(),getMateria()));
		}
	}
}
