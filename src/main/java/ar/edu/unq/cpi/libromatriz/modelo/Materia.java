package ar.edu.unq.cpi.libromatriz.modelo;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Materia extends Persistible {

	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn
	private Carrera carrera;

	private String nombre;

	private boolean esPromocionable;
	
	//relacion materia a materia...fech x default es lazy.. sino te trae toda la base de datos materia
	//sino hibernete te trae toda las materia
	//eager entidad fuerte a entidad debil
	
	@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REFRESH })
	@JoinTable(name="Materia_Correlativa")
	private Set<Materia> correlativas = new HashSet<>();

	public void baja(Materia materia) { // para verificar que esta la materia q
										// voy a eliminar
		if (carrera.getMaterias().contains(materia)) {
			carrera.getMaterias().remove(materia);
		}
	}

	public Materia() {
	}

	public Materia(String nombre, boolean esPromocionable) {
		this.nombre = nombre;
		this.esPromocionable = esPromocionable;
	}

	public Materia(Carrera carrera, String nombre, boolean esPromocionable) {
		super();
		this.carrera = carrera;
		this.nombre = nombre;
		this.esPromocionable = esPromocionable;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean esPromocionable() {
		return esPromocionable;
	}

	public void setEsPromocionable(boolean esPromocionable) {
		this.esPromocionable = esPromocionable;
	}

	public String getPromocionable() {
		if (this.esPromocionable == true) {
			return "Si";
		}
		return "No";
	}

	 public Set<Materia> getCorrelativas() {
	 return correlativas;
	 }
	
	 public void setCorrelativas(Set<Materia> correlativas) {
	 this.correlativas = correlativas;
	 }
	
	 public void borrarCorrelativa(Materia materia){
	 this.correlativas.remove(materia);
	 }
	
	 public void agregarCorrelativa(Materia materia){
	 this.correlativas.add(materia);
	 }

	public Carrera getCarrera() {
		return this.carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
}
