package ar.edu.unq.cpi.libromatriz.epers;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;

public class AlumnoHome extends HomeGeneralSession<Object> {

	private static final long serialVersionUID = 4775910097257163038L;

	static private AlumnoHome instance = new AlumnoHome();
	public static AlumnoHome getInstance() {
		return instance;
	}
	

	@Override
		public Alumno findByName(String name) {
			return this.getSession().createQuery("FROM Alumno WHERE nombre = :name", Alumno.class)
					.setParameter("name", name).getSingleResult();

		}


}
