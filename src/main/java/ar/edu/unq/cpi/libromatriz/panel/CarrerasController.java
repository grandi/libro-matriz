package ar.edu.unq.cpi.libromatriz.panel;

import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

public class CarrerasController {
	private Alumno alumno;
	private Carrera carrera;

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
	
	public List<Carrera> getCarreras(){
		return this.getAlumno().getCarreras();
	}
	
	
	
}
