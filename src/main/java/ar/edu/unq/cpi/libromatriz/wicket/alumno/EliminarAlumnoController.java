package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import java.io.Serializable;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Direccion;

public class EliminarAlumnoController implements Serializable{
	private static final long serialVersionUID = -9116638731672090631L;
	
	private Alumno alumno;
	private String nombre;
	private String apellido;
	private String dni;
	private Direccion direccion;
	private String calle;
	private String numero;
	private String localidad;
	private String telefono;
	private String email;
	
	public EliminarAlumnoController() {
		super();
	}
	
	public EliminarAlumnoController(Alumno _alumno) {
		this.alumno=_alumno;
		this.nombre=_alumno.getNombre();
		this.apellido=_alumno.getApellido();
		this.dni=_alumno.getDni();
		this.direccion=new Direccion(_alumno.getDireccion().getCalle(),_alumno.getDireccion().getNumero(),
				_alumno.getDireccion().getCalle());
		this.telefono=_alumno.getTelefono();
		this.email=_alumno.getEmail();
			
	}
	
	public Alumno getAlumno() {
		return alumno;
	}
	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public Direccion getDireccion() {
		return direccion;
	}
	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	public void eliminarAlumno(){
		AlumnoStore.unico().setBajaAlumno(alumno);
		
	}
	
}
