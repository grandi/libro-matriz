package ar.edu.unq.cpi.libromatriz.panel;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.EstadoCursada;
import ar.edu.unq.cpi.libromatriz.wicket.HomePage;
import ar.edu.unq.cpi.libromatriz.wicket.matriculacion.NoPudoMatricularsePage;
import ar.edu.unq.cpi.libromatriz.wicket.matriculacion.PudoMatricularsePage;

public class MateriasPanel extends Panel {
	private static final long serialVersionUID = -5564221165168905568L;
	private Carrera carrera;
	private MateriaController controller = new MateriaController();
	
	public MateriasPanel(String wicket_id) {
		super(wicket_id);
		this.setVisible(false);
		this.setOutputMarkupId(true);
		
		
		Form<MateriaController> buscarMateriaForm = new Form<MateriaController>("buscarMateriaForm");
		
		AjaxButton submitBtn = new AjaxButton("aceptar") {
			private static final long serialVersionUID = -7444106852272624717L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
					controller.matricular(new HomePage());
			}
			
		};
	
		
		buscarMateriaForm.add(new Label("nombreCarrera", new PropertyModel<>(controller, "carrera.nombre")));
		
		buscarMateriaForm.add(new DropDownChoice<>(
					// id
					"materia",
					// binding del valor
					new PropertyModel<>(this.controller, "materia"),
					// binding de la lista de items
					new PropertyModel<>(this.controller, "materiasNoCursadas"),
					// que se muestra de cada item
					new ChoiceRenderer<>("nombre")
			));		
		
		buscarMateriaForm.add(submitBtn);
		this.add(buscarMateriaForm);
	}

	public void setCarrera(Carrera carrera) {
		this.controller.setCarrera(carrera);
		
	}

	public void setAlumno(Alumno alumno) {
		this.controller.setAlumno(alumno);	
	}

	public MateriaController getController() {
		return controller;
	}
	
}
