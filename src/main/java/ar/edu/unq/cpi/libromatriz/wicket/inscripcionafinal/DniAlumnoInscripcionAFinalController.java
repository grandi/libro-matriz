package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.wicket.alumno.AlumnoStore;

public class DniAlumnoInscripcionAFinalController {

	private Alumno alumno;
	private String dni;
	
	public DniAlumnoInscripcionAFinalController() {
		Alumno mario = new Alumno("Mario", "Gudiño", "30773213");
		AlumnoStore.unico().agregarAlumno(mario);
	}
	
	public DniAlumnoInscripcionAFinalController(Alumno alumno){
		this.alumno = alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	public void setDni(String dni){
		this.dni = dni;
	}

	public String getDni() {
		return dni;
	}
	
	public Alumno getAlumno(){
		
		return AlumnoStore.unico().getAlumnoConDni(this.getDni());
	}
}
