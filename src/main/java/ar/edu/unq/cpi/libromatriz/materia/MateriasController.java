package ar.edu.unq.cpi.libromatriz.materia;

import java.io.Serializable;
import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;

public class MateriasController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Carrera carrera;

	public MateriasController(Carrera carrera) {
		this.carrera = carrera;
	}

	public List<Materia> getMaterias() {
		return carrera.getMaterias();
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public String getNombreCarrera() {
		return carrera.getNombre();
	}
	public void eliminarMateria(Materia materia){
		this.carrera.getListadoMaterias().remove(materia);
	}	
}