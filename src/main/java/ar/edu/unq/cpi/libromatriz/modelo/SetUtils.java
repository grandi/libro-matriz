package ar.edu.unq.cpi.libromatriz.modelo;

import java.util.HashSet;
import java.util.Set;

public class SetUtils {

	public static <T> Set<T> diferencia(Set<T> una, Set<T> otra) {
	    Set<T> tmp = new HashSet<T>(una);
	    tmp.removeAll(otra);
	    return tmp;
	}
}
