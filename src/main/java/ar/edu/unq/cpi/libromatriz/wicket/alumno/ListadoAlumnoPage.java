package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.wicket.BotonConfirmar;
import ar.edu.unq.cpi.libromatriz.wicket.HomePage;
import ar.edu.unq.cpi.libromatriz.wicket.carrera.CarreraController;

public class ListadoAlumnoPage extends WebPage {

	private static final long serialVersionUID = -4444795484673343576L;

	private ListadoAlumnoController controller = new ListadoAlumnoController();
	private CarreraController carrera;
	private List<Alumno> alumnos;
	
	
	public ListadoAlumnoPage() {
		super();
		this.controller = new ListadoAlumnoController();
		this.agregarListado();
		this.agregarBotonVolver();

	}
	public ListadoAlumnoPage(Carrera carrera){
		this.controller=new ListadoAlumnoController(carrera);
		this.agregarListado();
		this.agregarBotonVolver();
		
	}

	private void agregarListado() {
		this.add(new ListView<Alumno>("filaAlumno", new PropertyModel<>(this.controller, "alumnos")) {

			private static final long serialVersionUID = 2426749934569985837L;

			protected void populateItem(ListItem<Alumno> panel) {
				Alumno alumno = panel.getModelObject();
				CompoundPropertyModel<Alumno> alumnoModel = new CompoundPropertyModel<>(alumno);
				panel.add(new Label("nombreCompleto", alumnoModel.bind("nombreCompleto")));
				panel.add(new Label("dni", alumnoModel.bind("dni")));

				panel.add(new Link<String>("eliminarAlumno") {
					private static final long serialVersionUID = 8397905807894993988L;

					@Override
					public void onClick() {
						this.setResponsePage(new EliminarAlumnoPage(alumno));

					}

				});

				panel.add(new Link<String>("verDatos") {
					private static final long serialVersionUID = -6226604951323971752L;

					@Override
					public void onClick() {
						this.setResponsePage(new DatosAlumnoPage(alumno));

					}

				});

			};

		});
	}

	private void agregarBotonVolver() {
		this.add(new Link<String>("agregarAlumno") {

			private static final long serialVersionUID = -4267458418903930966L;

			@Override
			public void onClick() {
				this.setResponsePage(new AgregarAlumnoPage());

			}

		});

		
		this.add(new Link<String>("volver") {

			private static final long serialVersionUID = 505927122883116822L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}
		});

	}

}
