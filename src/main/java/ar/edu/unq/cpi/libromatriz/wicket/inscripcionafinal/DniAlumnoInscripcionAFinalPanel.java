package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

public class DniAlumnoInscripcionAFinalPanel extends Panel {
	private static final long serialVersionUID = -605063451474231087L;
	private CarrerasInscripcionAFinalPanel carrerasInscripcionAFinalPanel;
	private DniAlumnoInscripcionAFinalController alumnoController = new DniAlumnoInscripcionAFinalController();
	
	public DniAlumnoInscripcionAFinalPanel(String id) {
		super(id);
		
		Form<DniAlumnoInscripcionAFinalController> buscarAlumnoForm = new Form<DniAlumnoInscripcionAFinalController>("buscarAlumnoForm");
		
		AjaxButton verAlumno = new AjaxButton("verAlumno") {
			private static final long serialVersionUID = -7444106852272624717L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				carrerasInscripcionAFinalPanel.setAlumno(alumnoController.getAlumno());
				carrerasInscripcionAFinalPanel.setVisible(true);
				
				Page laPaginaQueMeContiene = target.getPage();
				laPaginaQueMeContiene.add(carrerasInscripcionAFinalPanel);
				target.add(laPaginaQueMeContiene);
			}
		};
		
		buscarAlumnoForm.add(verAlumno);
		buscarAlumnoForm.add(new TextField<>("dniInput", new PropertyModel<>(alumnoController,"dni")));
		this.add(buscarAlumnoForm);
	}	
	
	public void setCarrerasInscripcionAFinalPanel(CarrerasInscripcionAFinalPanel carrerasInscripcionAFinalPanel) {
		this.carrerasInscripcionAFinalPanel = carrerasInscripcionAFinalPanel;
	}

	public DniAlumnoInscripcionAFinalController getAlumnoController() {
		return alumnoController;
	}

}
