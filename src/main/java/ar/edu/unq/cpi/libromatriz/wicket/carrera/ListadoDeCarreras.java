package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.wicket.HomePage;

public class ListadoDeCarreras extends WebPage {

	private static final long serialVersionUID = 1L;
	private CarreraController controller;

	public ListadoDeCarreras() {
		super();
		this.controller = new CarreraController();
		this.tablaCarreras();
		this.botonAgregar();
	}

	public void tablaCarreras() {

		this.add(new ListView<Carrera>("lasCarreras", new PropertyModel<>(this.controller, "carreras")) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Carrera> item) {

				CompoundPropertyModel<Carrera> laCarrera = new CompoundPropertyModel<>(item.getModelObject());
				item.add(new Label("nombre", laCarrera.bind("nombre")));
				item.add(new Label("resolucion", laCarrera.bind("resolucion")));
				item.add(new Label("duracion", laCarrera.bind("duracion")));

				item.add(new Link<String>("archivarCarrera") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						this.setResponsePage(new ArchivarCarreraPage(item.getModelObject()));
					}

				});

				item.add(new Link<String>("detalleCarrera") {
					private static final long serialVersionUID = 1L;

					@Override
					public void onClick() {
						this.setResponsePage(new DetalleCarrera(item.getModelObject()));
					}

				});

			}
		});

	}

	public void botonAgregar() {
		this.add(new Link<String>("agregarCarrera") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new AgregarCarreraPage());

			}

		});
		this.add(new Link<String>("carrerasArchivadas") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoCarrerasArchivadas());

			}

		});
		this.add(new Link<String>("volver") {

			private static final long serialVersionUID = 505927122883116822L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}
		});

	}
	

}
