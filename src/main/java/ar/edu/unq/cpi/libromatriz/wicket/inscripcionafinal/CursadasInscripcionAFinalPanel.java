package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;

public class CursadasInscripcionAFinalPanel extends Panel {
	private static final long serialVersionUID = -619223567601414537L;
	private ExamenInscripcionAFinalPanel examenInscripcionAFinalPanel;
	
	private CursadasInscripcionAFinalController controller = new CursadasInscripcionAFinalController();

	public CursadasInscripcionAFinalPanel(String id) {
		super(id);
		this.setVisible(false);
		this.setOutputMarkupId(true);
		
		Form<CursadasInscripcionAFinalController> buscarCursadaForm = new Form<CursadasInscripcionAFinalController>("buscarCursadaForm");
		
		buscarCursadaForm.add(new Label("nombreCarrera", new PropertyModel<>(this.controller, "carrera.nombre")));
		
		buscarCursadaForm.add(new DropDownChoice<>(
				// id
				"cursada",
				// binding del valor
				new PropertyModel<>(this.controller, "cursada"),
				// binding de la lista de items
				new PropertyModel<>(this.controller, "cursadasEnCursoORegularizadas"),
				// que se muestra de cada item
				new ChoiceRenderer<>("materia.nombre")
		));
		
		AjaxButton verCursada = new AjaxButton("verCursada") {
			private static final long serialVersionUID = 3746969647126748997L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				examenInscripcionAFinalPanel.setCarrera(controller.getCarrera());
				examenInscripcionAFinalPanel.setAlumno(controller.getAlumno());
				examenInscripcionAFinalPanel.setVisible(true);
				
				Page laPaginaQueMeContiene = target.getPage();
				laPaginaQueMeContiene.add(examenInscripcionAFinalPanel);
				target.add(laPaginaQueMeContiene);
			}
		};
		
		buscarCursadaForm.add(verCursada);
		this.add(buscarCursadaForm);
	}

	public ExamenInscripcionAFinalPanel getExamenInscripcionAFinalPanel() {
		return examenInscripcionAFinalPanel;
	}
	
	public void setExamenInscripcionAFinalPanel(ExamenInscripcionAFinalPanel examenInscripcionAFinalPanel) {
		this.examenInscripcionAFinalPanel = examenInscripcionAFinalPanel;
	}	

	public void setAlumno(Alumno alumno) {
		this.controller.setAlumno(alumno);	
	}

	public Alumno getAlumno() {
		return this.controller.getAlumno();
	}

	public void setCarrera(Carrera carrera) {
		this.controller.setCarrera(carrera);
	}

}

