package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import java.util.ArrayList;
import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Cursada;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class CursadasInscripcionAFinalController {
	private Carrera carrera;
	private Cursada cursada;
	private Alumno alumno;
	
	public CursadasInscripcionAFinalController() {
		super();
	}

	public CursadasInscripcionAFinalController(Carrera carrera, Cursada cursada, Alumno alumno) {
		super();
		this.carrera = carrera;
		this.cursada = cursada;
		this.alumno = alumno;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

	public Cursada getCursada() {
		return cursada;
	}

	public void setCursada(Cursada cursada) {
		this.cursada = cursada;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}
	
	public List<Cursada> getCursadasEnCursoORegularizadas(){
		List<Cursada> listaDeCursadas = new ArrayList<>(this.getAlumno().getCursadasEnCursoORegularizadasPor(this.getAlumno().getDni()));
		return listaDeCursadas;
	}
	
}

