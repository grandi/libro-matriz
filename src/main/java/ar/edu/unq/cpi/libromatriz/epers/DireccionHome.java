package ar.edu.unq.cpi.libromatriz.epers;

import ar.edu.unq.cpi.libromatriz.modelo.Direccion;

public class DireccionHome extends HomeGeneralSession<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static private DireccionHome instance = new DireccionHome();

	public static DireccionHome getInstance() {
		return instance;
	}

	@Override
	public Direccion findByName(String name) {
		return this.getSession().createQuery("FROM Direccion WHERE nombre = :name", Direccion.class)
				.setParameter("name", name).getSingleResult();

	}

}
