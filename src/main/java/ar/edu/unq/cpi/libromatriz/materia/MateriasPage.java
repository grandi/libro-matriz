package ar.edu.unq.cpi.libromatriz.materia;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.wicket.carrera.DetalleCarrera;
import ar.edu.unq.cpi.libromatriz.wicket.correlatividad.CorrelativasPage;

public class MateriasPage extends WebPage {

	private static final long serialVersionUID = -1061824403496088979L;

	private MateriasController controller;

	private Carrera carrera;
	public MateriasPage(Carrera carrera) {
		this.carrera = carrera;
		this.controller = new MateriasController(carrera);
		this.nombreDeMateria();
		this.agregarTablaDeMaterias();
		this.agregarBotones();
		
		this.add(new Link<String>("crearMateria") {
			private static final long serialVersionUID = -510395408463432819L;

			@Override
			public void onClick() {
				this.setResponsePage(new CrearMateria(carrera));
			}
		});

	}

	private void agregarBotones() {
			this.add(new Link<String>("volver") {
				private static final long serialVersionUID = -5103954084634722612L;

				@Override
				public void onClick() {
					this.setResponsePage(new DetalleCarrera(carrera));
				}
			});
		}
	
		
	

	private void nombreDeMateria() {
		this.add(new Label("nombreMateria", new PropertyModel<>(this.controller, "nombreCarrera")));
	}
	

	private void agregarTablaDeMaterias() {
		this.add(new ListView<Materia>("filaMateria", new PropertyModel<>(this.controller, "materias")) {

			private static final long serialVersionUID = 2426749934569985837L;

			protected void populateItem(ListItem<Materia> panel) {
				Materia materia = panel.getModelObject();
				CompoundPropertyModel<Materia> materiaModel = new CompoundPropertyModel<>(materia);
				panel.add(new Label("nombre", materiaModel.bind("nombre")));
				panel.add(new Label("esPromocionable", materiaModel.bind("getPromocionable")));

				Link<String> botonAsignar = new Link<String>("modificar") {
					private static final long serialVersionUID = 3672370417232954427L;

					@Override
					public void onClick() {

						this.setResponsePage(new ModificarMateriaPage(materia, carrera));
					}
				};
				Link<String> botonCorrelativas = new Link<String>("correlativas") {
					private static final long serialVersionUID = 3672370417232954427L;

					@Override
					public void onClick() {

						this.setResponsePage(new CorrelativasPage(materia));
					}
				};
				Link<String> botonEliminar = new Link<String>("eliminar") {
					private static final long serialVersionUID = 3672370417232954427L;

					@Override
					public void onClick() {

						controller.eliminarMateria(materia);
					}

				
				};
				panel.add(botonAsignar);
				panel.add(botonCorrelativas);
				panel.add(botonEliminar);
			};
		

		});
		

	}
}