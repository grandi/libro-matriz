package ar.edu.unq.cpi.libromatriz.wicket.correlatividad;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class MateriaElegidaController implements Serializable {


	private static final long serialVersionUID = 1L;
	private Materia materiaElegida;
	private List<Materia> correlativas = new ArrayList<>();
	private List<Materia> materiasCarrera = new ArrayList<>();

	private Materia nuevaCorrelativa;
	
	public MateriaElegidaController(Materia materiaE) {
		this.materiaElegida = materiaE;
		this.correlativas = new ArrayList<>(materiaE.getCorrelativas());
		this.materiasCarrera.addAll(materiaE.getCarrera().getMaterias());
		this.materiasCarrera.removeAll(correlativas);
		this.materiasCarrera.remove(this.materiaElegida);
	}

	public Materia getMateriaElegida() {
		return materiaElegida;
	}

	public void setMateriaElegida(Materia materiaElegida) {
		this.materiaElegida = materiaElegida;
	}
	public List<Materia> getCorrelativas(){
		return this.correlativas;
	}
	
	public void setCorrelativa(Materia materia){
		this.correlativas.add(materia);
	}
	
	public void confirmarAgregarCorrelativa(){
		materiaElegida.agregarCorrelativa(this.nuevaCorrelativa);
	}
	
	public Materia getCorrelativaNueva() {
		return nuevaCorrelativa;
	}

	public void setCorrelativaNueva(Materia correlativaNueva) {
		this.nuevaCorrelativa = correlativaNueva;
	}
	
	public void borrarCorrelativa(Materia materia){
		if (this.correlativas.contains(materia)){
			this.correlativas.remove(materia);
			this.materiasCarrera = materiaElegida.getCarrera().getMaterias();
			this.materiasCarrera.removeAll(correlativas);
			this.materiasCarrera.remove(materiaElegida);
		}
	}
	
	public List<Materia> getMateriasCarrera() {
		return materiasCarrera;
	}

	public Materia getNuevaCorrelativa() {
		return nuevaCorrelativa;
	}

	public void setNuevaCorrelativa(Materia nuevaCorrelativa) {
		this.nuevaCorrelativa = nuevaCorrelativa;
	}
}
