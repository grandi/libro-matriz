package ar.edu.unq.cpi.libromatriz.epers;

import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class MateriaHome extends HomeGeneralSession<Object>{

	private static final long serialVersionUID = 4775910097257163038L;

	static private MateriaHome instance = new MateriaHome();

	public static MateriaHome getInstance() {
		return instance;
	}

	@Override
	public Materia findByName(String name) {
		return this.getSession().createQuery("FROM Materia WHERE nombre = :name", Materia.class)
				.setParameter("name", name).getSingleResult();
	}

}
