package ar.edu.unq.cpi.libromatriz.panel;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;

public class CarrerasPanel extends Panel {
	private static final long serialVersionUID = -5564221165168905568L;
	private MateriasPanel materiasPanel;
	private CarrerasController controller = new CarrerasController();
	
	public CarrerasPanel(String wicket_id) {
		super(wicket_id);
		
		this.setVisible(false);
		this.setOutputMarkupId(true);
		
		Form<CarrerasController> buscarCarreraForm = new Form<CarrerasController>("buscarCarreraForm");
		
		buscarCarreraForm.add(new Label("nombreAlumno", new PropertyModel<>(this.controller, "alumno.nombreCompleto")));
		
		buscarCarreraForm.add(new DropDownChoice<>(
				// id
				"carrera",
				// binding del valor
				new PropertyModel<>(this.controller, "carrera"),
				// binding de la lista de items
				new PropertyModel<>(this.controller, "carreras"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre")
		));
		
		AjaxButton verCarrera = new AjaxButton("verCarrera") {
			private static final long serialVersionUID = -7444106852272624717L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				materiasPanel.setCarrera(controller.getCarrera());
				materiasPanel.setAlumno(controller.getAlumno());
				materiasPanel.setVisible(true);
				
				Page laPaginaQueMeContiene = target.getPage();
				laPaginaQueMeContiene.add(materiasPanel);
				target.add(laPaginaQueMeContiene);
			}
		};
		
		buscarCarreraForm.add(verCarrera);
		this.add(buscarCarreraForm);
	}

	public void setMateriasPanel(MateriasPanel materiasPanel) {
		this.materiasPanel = materiasPanel;
	}

	public void setAlumno(Alumno alumno) {
		this.controller.setAlumno(alumno);
		
	}

	public Alumno getAlumno() {
		return this.controller.getAlumno();
	}
	
		
	
}
