package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

public class AgregarCarreraPage extends WebPage {

	private static final long serialVersionUID = 1L;
	private CarreraController controller = new CarreraController();

	public AgregarCarreraPage() {
		super();
		this.formularioAltaCarrera();

	}

	public void formularioAltaCarrera() {
		Form<CarreraController> altaCarrera = new Form<CarreraController>("laCarrera") {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				AgregarCarreraPage.this.controller.agregarCarrera();
				this.setResponsePage(new ListadoDeCarreras());

			}
		};
		altaCarrera.add(new Link<String>("cancelar") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());

			}

		});
		altaCarrera.add(new Link<String>("agregarLaMateria") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				// AgregarCarreraPage.this.controller.getMaterias();
				// this.setResponsePage(new
				// MateriaPage(controller.getCarrera()));
			}

		});

		altaCarrera.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		altaCarrera.add(new TextField<>("resolucion", new PropertyModel<>(this.controller, "resolucion")));
		altaCarrera.add(new TextField<>("duracion", new PropertyModel<>(this.controller, "duracion")));

		this.add(altaCarrera);

	}

}
