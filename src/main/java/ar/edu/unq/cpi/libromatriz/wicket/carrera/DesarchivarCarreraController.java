package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import java.io.Serializable;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;

public class DesarchivarCarreraController implements Serializable {

	private static final long serialVersionUID = 1L;
	private Carrera carrera;
	private String nombre;
	private String resolucion;
	private double duracion;

	public DesarchivarCarreraController() {
	}

	public DesarchivarCarreraController(Carrera carrera) {
		this.carrera = carrera;
		this.nombre = carrera.getNombre();
		this.resolucion = carrera.getResolucion();
		this.duracion = carrera.getDuracion();
	}

	public void activarCarreraArchivada() {
		OfertaAcademica.instituto().activarCarreraArchivada(carrera);
	}
}
