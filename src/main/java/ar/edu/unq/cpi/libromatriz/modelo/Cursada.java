package ar.edu.unq.cpi.libromatriz.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Cursada extends Persistible {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Materia materia;

	private double notaFinal;

	private EstadoCursada estado;

	private int anio;
	// private EstadoCursada estadoCursada;

	@ManyToMany(mappedBy = "cursadas")
	// cambie nombre de la lista a cursadas
	private List<Alumno> cursadaDelAlumno = new ArrayList<>();

	public void setCursadas(List<Alumno> cursadas) {
		this.cursadaDelAlumno = cursadas;
	}

	public List<Alumno> getCursadas() {
		return cursadaDelAlumno;
	}

	public Cursada() {
	}

	public Cursada(EstadoCursada _estadoCursada) {
		this.estado = _estadoCursada;
	}

	public Cursada(Materia materia, EstadoCursada estado) {
		super();
		this.materia = materia;
		this.estado = estado;
	}

	public void setEstado(EstadoCursada _estadoCursada) {
		this.estado = (_estadoCursada);
	}

	public EstadoCursada getEstado() {
		return estado;
	}

	public void addQuienHizoCursada(Alumno _alumno) {
		this.cursadaDelAlumno.add(_alumno);

	}

	public boolean cursaAlumnoConDni(String _dni) {
		return cursadaDelAlumno.stream().anyMatch(a -> a.getDni().equals(_dni));
	}

	public boolean estadoCursando() {// cambie el nombre del metodo
		return this.getEstado().equals(EstadoCursada.CURSANDO);
	}

	public boolean estadoRegularizadoOAprobado() {
		return (this.getEstado().equals(EstadoCursada.REGULARIZADA)
				|| this.getEstado().equals(EstadoCursada.APROBADA_FINAL)
				|| this.getEstado().equals(EstadoCursada.APROBADA_PROMOCION));
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public boolean estaRegularizada() {
		return this.getEstado().equals(EstadoCursada.REGULARIZADA);
	}

	public boolean estaAprobada() {
		return (this.getEstado().equals(EstadoCursada.APROBADA_PROMOCION)
				|| this.getEstado().equals(EstadoCursada.APROBADA_FINAL));
	}

	public boolean estaDesaprobada() {
		return this.getEstado().equals(EstadoCursada.DESAPROBADA);
	}

}
