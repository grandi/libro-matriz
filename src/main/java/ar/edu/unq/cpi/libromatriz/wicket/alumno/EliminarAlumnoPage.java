package ar.edu.unq.cpi.libromatriz.wicket.alumno;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;


import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.wicket.BotonConfirmar;



public class EliminarAlumnoPage extends WebPage{
	private static final long serialVersionUID = 8875660224713412965L;
	private EliminarAlumnoController controller;
	
	public EliminarAlumnoPage() {
		super();
	}
	public EliminarAlumnoPage(Alumno _alumno){
		this.controller= new EliminarAlumnoController(_alumno);
		this.eliminarAlumno();
	
	}
	
	public void eliminarAlumno() {
		Form<EliminarAlumnoController>eliminarAlumnoForm= new Form<EliminarAlumnoController>
		("eliminarAlumnoForm"){
			private static final long serialVersionUID = 1L;
			
			@Override
			public void onSubmit(){
				EliminarAlumnoPage.this.controller.eliminarAlumno();
				this.setResponsePage(new ListadoAlumnoPage());
			}		
		};
		eliminarAlumnoForm.add(new BotonConfirmar("submit", "Está seguro que desea eliminar los datos del alumno?"));
		eliminarAlumnoForm.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		eliminarAlumnoForm.add(new TextField<>("apellido", new PropertyModel<>(this.controller, "apellido")));
		eliminarAlumnoForm.add(new TextField<>("dni", new PropertyModel<>(this.controller,"dni")));
		eliminarAlumnoForm.add(new TextField<>("telefono",new PropertyModel<>(this.controller,"telefono")));
		eliminarAlumnoForm.add(new TextField<>("calle",new PropertyModel<>(this.controller,"direccion.calle")));
		eliminarAlumnoForm.add(new TextField<>("numero",new PropertyModel<>(this.controller, "direccion.numero")));
		eliminarAlumnoForm.add(new TextField<>("localidad",new PropertyModel<>(this.controller, "direccion.localidad")));
		eliminarAlumnoForm.add(new TextField<>("email",new PropertyModel<>(this.controller, "email")));
				
		this.add(eliminarAlumnoForm);
		
        eliminarAlumnoForm.add(new Link<String>("cancelar") {
			private static final long serialVersionUID = 1L;

	   @Override
	   public void onClick() {
		this.setResponsePage(new ListadoAlumnoPage());}

      });
  }
}
