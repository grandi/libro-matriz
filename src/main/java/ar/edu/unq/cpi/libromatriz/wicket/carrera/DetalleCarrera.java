package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.materia.MateriasPage;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.wicket.alumno.ListadoAlumnoPage;

public class DetalleCarrera extends WebPage {

	private static final long serialVersionUID = 1L;
	private CarreraController controller;

	private Carrera carreraNaty;

	public DetalleCarrera(Carrera carrera1) {
		this.carreraNaty = carrera1;
		this.controller = new CarreraController(carrera1);
		this.detalleCarrera();
	}

	public void detalleCarrera() {
		Form<CarreraController> carrera = new Form<CarreraController>("detalleCarrera") {

			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				DetalleCarrera.this.controller.modificarCarrera();
				this.setResponsePage(new ListadoDeCarreras());
			}
		};
		carrera.add(new Link<String>("ListadoAlumnosNaty") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoAlumnoPage(carreraNaty));

			}
		});
		carrera.add(new Link<String>("ListadoMateriasNAHUEL") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new MateriasPage(carreraNaty));

			}
		});
		carrera.add(new Link<String>("cancelar") {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());

			}

		});

		carrera.add(new TextField<>("nombre", new PropertyModel<>(this.controller, "nombre")));
		carrera.add(new TextField<>("resolucion", new PropertyModel<>(this.controller, "resolucion")));
		carrera.add(new TextField<>("duracion", new PropertyModel<>(this.controller, "duracion")));

		this.add(carrera);
	}

}
