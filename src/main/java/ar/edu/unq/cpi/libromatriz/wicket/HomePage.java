package ar.edu.unq.cpi.libromatriz.wicket;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.resource.PackageResourceReference;

import ar.edu.unq.cpi.libromatriz.materia.MateriasPage;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;
import ar.edu.unq.cpi.libromatriz.wicket.alumno.ListadoAlumnoPage;
import ar.edu.unq.cpi.libromatriz.wicket.carrera.ListadoDeCarreras;
import ar.edu.unq.cpi.libromatriz.wicket.correlatividad.CorrelativasPage;
import ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal.FormInscripcionAFinal;
import ar.edu.unq.cpi.libromatriz.wicket.matriculacion.FormMatriculacion;

public class HomePage extends WebPage {

	private static final long serialVersionUID = 1L;

	public HomePage() {
		this.add(new Image("image", new PackageResourceReference(HomePage.class, "logoEscuela.png")));
		
		this.add(new Link<String>("matriculacion") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new FormMatriculacion());
			}

		});

		this.add(new Link<String>("inscripcionAFinal") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new FormInscripcionAFinal());
			}

		});

		this.add(new Link<String>("listadoDeCarreras") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoDeCarreras());
			}

		});


		this.add(new Link<String>("materiasDeTPI") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(
						new MateriasPage(OfertaAcademica.instituto().getCarreraConNombre("tecnicatura en informatica")));
			}

		});

		this.add(new Link<String>("listadoAlumnos") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new ListadoAlumnoPage());
			}

		});

		this.add(new Link<String>("correlatividades") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(
						new CorrelativasPage(OfertaAcademica.instituto().getCarreraConNombre("tecnicatura en informatica").getMateriaConNombre("Programación con objetos I")));
			}
		});
	}
}
