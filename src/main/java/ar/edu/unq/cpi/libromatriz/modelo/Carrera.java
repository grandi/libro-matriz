package ar.edu.unq.cpi.libromatriz.modelo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Carrera extends Persistible {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String resolucion;

	private String nombre;

	private double duracion;

	// private Materia materia;
	@OneToMany(mappedBy = "carrera", cascade = CascadeType.ALL)
	private List<Materia> listadoMaterias = new ArrayList<>();

	// @ManyToMany(mappedBy = "carreras")
	// @Column(name = "CARRERA_ALUMNO", nullable = true)
	// @Transient para q no tome en cuenta esta relacion
	@OneToMany(mappedBy = "carrera")
	private List<Alumno> alumnosInscriptos = new ArrayList<>();

	public Carrera(String string) {
	}

	public Carrera() {
	}

	public Carrera(String nombre, String resolucion, double duracion) {
		super();
		this.nombre = nombre;
		this.resolucion = resolucion;
		this.duracion = duracion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getResolucion() {
		return resolucion;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public double getDuracion() {
		return duracion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	public List<Materia> getMaterias() {
		return listadoMaterias;
	}

	public boolean yaFueCargadaMateria(String nombre) {
		return listadoMaterias.stream().anyMatch(m -> m.getNombre() == nombre);
	}

	public boolean puedoAgregarMateria(Materia materia) {
		return !yaFueCargadaMateria(materia.getNombre());
	}

	public void agregarMateria(Materia materia) {
		if (puedoAgregarMateria(materia)) {
			this.listadoMaterias.add(materia);
			materia.setCarrera(this);
		}
	}

	// public Materia getMateria() {
	// return materia;
	// }
	//
	// public void setMateria(Materia materia) {
	// this.materia = materia;
	// }

	public List<Materia> getListadoMaterias() {
		return listadoMaterias;
	}

	public void setAlumnosInscriptos(List<Alumno> alumnosInscriptos) {
		this.alumnosInscriptos = alumnosInscriptos;
	}

	public List<Alumno> getAlumnosInscriptos() {
		return alumnosInscriptos;
	}

	// public void setAlumnosInscriptos(Alumno alumnosInscriptos) {
	// this.alumnosInscriptos.add(alumnosInscriptos);
	// }

	public Materia getMateriaConNombre(String name) {
		return listadoMaterias.stream().filter(c -> c.getNombre().toLowerCase().equals(name.toLowerCase())).findAny()
				.get();
	}

	public void agregarAlumno(Alumno alumno) {
		this.alumnosInscriptos.add(alumno);
	}

	public boolean contieneMateria(Materia materia) {
		return this.getMaterias().contains(materia);
	}

	public void setListadoMaterias(List<Materia> listadoMaterias) {
		this.listadoMaterias = listadoMaterias;
	}
	// public Alumno getAlumno() {
	// return alumno;
	// }
	//
	// public void setAlumno(Alumno alumno) {
	// this.alumno = alumno;
	// }
}
