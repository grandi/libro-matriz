package ar.edu.unq.cpi.libromatriz.wicket.carrera;

import java.util.List;
import java.io.Serializable;
import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.OfertaAcademica;

public class CarreraController implements Serializable {
	private static final long serialVersionUID = 1L;

	private Carrera carrera;
	private String nombre;
	private String resolucion;
	private double duracion;

	public CarreraController() {
	}

	public CarreraController(Carrera carrera) {
		this.carrera = carrera;
		this.nombre = carrera.getNombre();
		this.resolucion = carrera.getResolucion();
		this.duracion = carrera.getDuracion();
	}

	public Carrera setCarrera(Carrera carrera) {
		return this.carrera = carrera;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setResolucion(String resolucion) {
		this.resolucion = resolucion;
	}

	public void setDuracion(double duracion) {
		this.duracion = duracion;
	}

	public void agregarCarrera() {
		if (this.getNombre() != null && this.getResolucion() != null && this.getDuracion() != 0) {
			Carrera _carrera = new Carrera(this.nombre, this.resolucion, this.duracion);

			OfertaAcademica.instituto().addCarreraAlListado(_carrera);
		}
	}

	public String getNombre() {
		return nombre;
	}

	public String getResolucion() {

		return resolucion;
	}

	public double getDuracion() {
		return duracion;
	}

	public List<Carrera> getCarreras() {
		return OfertaAcademica.instituto().getCarreras();
	}

	public void modificarCarrera() {
		if (this.getNombre() != null && this.getResolucion() != null && this.getDuracion() != 0) {
			this.carrera.setNombre(this.getNombre());
			this.carrera.setResolucion(this.getResolucion());
			this.carrera.setDuracion(this.getDuracion());
		}
	}

	public List<Carrera> getCarrerasArchivadas() {
		return OfertaAcademica.instituto().getCarrerasArchivadas();
	}

}
