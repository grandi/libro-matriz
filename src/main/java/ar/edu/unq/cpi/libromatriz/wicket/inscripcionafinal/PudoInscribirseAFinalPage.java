package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;
import ar.edu.unq.cpi.libromatriz.wicket.HomePage;

public class PudoInscribirseAFinalPage extends WebPage {

	private static final long serialVersionUID = 1L;
	
	public PudoInscribirseAFinalPage(Alumno alumno, Materia materia){
		super();
		
		this.add(new Label("nomAlumno", new PropertyModel<>(alumno, "nombreCompleto")));
		this.add(new Label("nomMateria", new PropertyModel<>(materia, "nombre")));
		
		this.add(new Link<String>("homeBtn") {
			private static final long serialVersionUID = 1L;

			@Override
			public void onClick() {
				this.setResponsePage(new HomePage());
			}
		});
	}
}
