package ar.edu.unq.cpi.libromatriz.wicket.matriculacion;

import java.util.List;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class CrearMatriculacionController {
	
	private Materia materia;
	private Carrera carrera;

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}
	
//	public List<Materia> getMateriasNoRegularizadas(){
//		return getCarrera().getMaterias.stream.filter(m -> m.getMateriasNoRegularizadas).Collectors.toset();
//	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}
}
