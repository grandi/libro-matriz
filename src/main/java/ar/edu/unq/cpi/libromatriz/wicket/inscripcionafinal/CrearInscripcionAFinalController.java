package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class CrearInscripcionAFinalController {
	
	private Materia	materia;
	private Carrera carrera;

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Carrera getCarrera() {
		return carrera;
	}

	public void setCarrera(Carrera carrera) {
		this.carrera = carrera;
	}

}
