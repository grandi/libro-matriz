package ar.edu.unq.cpi.libromatriz.modelo;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Direccion extends Persistible {

	private static final long serialVersionUID = 1L;

	private String calle;

	private String numero;

	private String localidad;

	@OneToOne(mappedBy = "direccion")
	private Docente docente;

	public Direccion(String _calle, String _numero, String _localidad) {
		this.calle = _calle;
		this.numero = _numero;
		this.localidad = _localidad;

	}

	public Direccion() {
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public Docente getDocente() {
		return docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

}
