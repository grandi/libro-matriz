package ar.edu.unq.cpi.libromatriz.materia;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Carrera;
import ar.edu.unq.cpi.libromatriz.modelo.Materia;

public class ModificarMateriaPage extends WebPage {
	private static final long serialVersionUID = 3341132595660337349L;
	private ModificarMateriaController controller = new ModificarMateriaController();
	private Materia materia;
	private Carrera carrera;

	public ModificarMateriaPage(Materia materia, Carrera carrera) {
		super();
		this.carrera = carrera;
		this.materia = materia;
		this.agregarForm();
		this.agregarBotones();
	}

	private void agregarForm() {
		Form<CrearMateriaController> crearViajeForm = new Form<CrearMateriaController>("materiaForm") {
			private static final long serialVersionUID = -1309536194793150773L;

			@Override
			protected void onSubmit() {
				// accion de negocios
				ModificarMateriaPage.this.controller.asignarMateria(materia);
				// navegacion
				this.setResponsePage(new MateriasPage(carrera));
			}
		};

		crearViajeForm.add(new TextField<>("nombre", new PropertyModel<>(this.materia, "nombre")));
		crearViajeForm.add(new TextField<>("esPromocionable", new PropertyModel<>(this.materia, "esPromocionable")));

		this.add(crearViajeForm);
	}

	private void agregarBotones() {
		this.add(new Link<String>("volver") {
			private static final long serialVersionUID = -5103954084634722612L;

			@Override
			public void onClick() {
				this.setResponsePage(new MateriasPage(carrera));
			}
		});
	}

}
