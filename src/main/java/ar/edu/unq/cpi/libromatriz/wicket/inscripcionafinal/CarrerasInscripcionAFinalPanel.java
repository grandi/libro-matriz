package ar.edu.unq.cpi.libromatriz.wicket.inscripcionafinal;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;

import ar.edu.unq.cpi.libromatriz.modelo.Alumno;

public class CarrerasInscripcionAFinalPanel extends Panel {
	private static final long serialVersionUID = -5046279975007664004L;
	private CursadasInscripcionAFinalPanel cursadasInscripcionAFinalPanel;

	private CarrerasInscripcionAFinalController controller = new CarrerasInscripcionAFinalController();

	public CarrerasInscripcionAFinalPanel(String id) {
		super(id);
		this.setVisible(false);
		this.setOutputMarkupId(true);
		
		Form<CarrerasInscripcionAFinalController> buscarCarreraForm = new Form<CarrerasInscripcionAFinalController>("buscarCarreraForm");
		
		buscarCarreraForm.add(new Label("nombreAlumno", new PropertyModel<>(this.controller, "alumno.nombreCompleto")));
		
		buscarCarreraForm.add(new DropDownChoice<>(
				// id
				"carrera",
				// binding del valor
				new PropertyModel<>(this.controller, "carrera"),
				// binding de la lista de items
				new PropertyModel<>(this.controller, "carreras"),
				// que se muestra de cada item
				new ChoiceRenderer<>("nombre")
		));
		
		AjaxButton verCarrera = new AjaxButton("verCarrera") {
			private static final long serialVersionUID = 3746969647126748997L;

			@Override
			protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
				cursadasInscripcionAFinalPanel.setCarrera(controller.getCarrera());
				cursadasInscripcionAFinalPanel.setAlumno(controller.getAlumno());
				cursadasInscripcionAFinalPanel.setVisible(true);
				
				Page laPaginaQueMeContiene = target.getPage();
				laPaginaQueMeContiene.add(cursadasInscripcionAFinalPanel);
				target.add(laPaginaQueMeContiene);
			}
		};
		
		buscarCarreraForm.add(verCarrera);
		this.add(buscarCarreraForm);
	}

	public CursadasInscripcionAFinalPanel getCursadasInscripcionAFinalPanel() {
		return cursadasInscripcionAFinalPanel;
	}
		
	public void setCursadasInscripcionAFinalPanel(CursadasInscripcionAFinalPanel cursadasInscripcionAFinalPanel) {
		this.cursadasInscripcionAFinalPanel = cursadasInscripcionAFinalPanel;
	}

	public void setAlumno(Alumno alumno) {
		this.controller.setAlumno(alumno);	
	}

	public Alumno getAlumno() {
		return this.controller.getAlumno();
	}

}
